<?php
// Routes

$app->get('/empleado', function ($request, $response, $args) {
    $this->logger->info("empleado '/' route");
    $model = new \Backend\models\Empleado();
    $data = json_decode($model->getList(), true);

	$email = $request->getParam('txtEmail');
	if (!empty($email)) { 
		$newData = [];
		foreach ( $data as $v) {
			if (strpos($v['email'], $email) !== false) {
				$newData[] = $v;
			}
		}
		$data = $newData;
		$args['txtEmail'] = $email;
	}
	
    return $this->renderer->render($response, 'empleado/index.phtml', [
        'args' => $args, 'empleado' => $data
    ]);
});

$app->get('/empleado/{id}', function ($request, $response, $args) {

    $this->logger->info("view empleado '/' route");

    if(null !== $request->getParam('v')) {
    	$data = unserialize(base64_decode($request->getParam('v')));
    }
    return $this->renderer->render($response, 'empleado/view.phtml', [
        'args' => $args, 'empleado' => $data
    ]);
});

$app->get('/servempleado/{min}/{max}', function ($request, $response, $args) use ($app) {

    $model = new \Backend\models\Empleado();
    $data = json_decode($model->getList(), true);
    $minimo = $args['min'];
    $maximo = $args['max'];
	$newData = [];
	$xml = '<xml>';

	foreach ( $data as $v) {

		$salary = floatval(str_replace(',', '', trim($v['salary'], '$')));
		if ($salary >= $minimo && $salary <= $maximo ) {
			$xml .= '<empleado>';
			$xml .= '<id>' . $v['id'] . '</id>';
			$xml .= '<name>' . $v['name'] . '</name>';
			$xml .= '<email>' . $v['email'] . '</email>';
			$xml .= '<position>' . $v['position'] . '</position>';
			$xml .= '<salary>' . $v['salary'] . '</salary>';
			$xml .= '</empleado>';
			$newData[] = $v;
		}
	}

	$xml .= '</xml>';
	echo $xml;
    return $response->withHeader('Content-type', 'application/xml');
});



$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
