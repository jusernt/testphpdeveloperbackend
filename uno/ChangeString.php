<?php

class ChangeString
{
	public function build ($cadena)
	{
		$cadena = trim ($cadena);
		$arr = str_split($cadena);
		$salida = '';

		$abcM = [
			 'a', 'b', 'c', 'd', 'e', 'f'
			 , 'g', 'h', 'i', 'j', 'k'
			 , 'l', 'm', 'n', 'ñ', 'o'
			 , 'p', 'q', 'r', 's', 't'
			 , 'u', 'v', 'w', 'x', 'y'
			 , 'z', 'a'

			 ,'A', 'B', 'C', 'D', 'E', 'F'
			 , 'G', 'H', 'I', 'J', 'K'
			 , 'L', 'M', 'N', 'Ñ', 'O'
			 , 'P', 'Q', 'R', 'S', 'T'
			 , 'U', 'V', 'W', 'X', 'Y'
			 , 'Z', 'A'
		];

		foreach ($arr as $v) {
			if (in_array($v, $abcM)) {
				$key = array_search ($v, $abcM);
				$salida .= $abcM[$key + 1];	
			} else {
				$salida .= $v;
			}
		}
		return $salida;
	}

}


