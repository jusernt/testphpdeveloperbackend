<?php 

include 'ChangeString.php';
include 'CompleteRange.php';
include 'ClearPar.php';

$uno1 = new ChangeString();
$uno2 = new CompleteRange();
$uno3 = new ClearPar();

echo '<pre>';
/*
"123 abcd*3"   salida : "123 ​bcde​*3"
"**Casa 52"   salida : "**​Dbtb​ 52"
"**Casa 52Z"   salida : "**​Dbtb​ 52​A​" 
*/
echo $uno1->build('**Casa 52Z');
echo '<br><br>';
/*
 [1, 2, 4, 5]   salida : [1, 2, ​3​,​ 4, 5] 
 [2, 4, 9]   salida : [2, ​3​,​ 4, ​5, 6, 7, 8​, 9] 
 [55, 58, 60]   salida : [55, ​56, 57, ​58,​ 59,​ 60] 
*/
print_r($uno2->build([2, 4, 9]));
echo '<br><br>';
/*
"()())()"  salida : "()()()"
"()(()"   salida : "()()"
")("   salida : ""
"((()"   salida : "()" 
*/
echo $uno3->build('((()');

echo '</pre>';


